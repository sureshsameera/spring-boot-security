package com.autusa.usersdemo.controller;

import com.autusa.usersdemo.model.User;
import com.autusa.usersdemo.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String viewHomePage() {
        return "index";
    }

    @GetMapping("/register")
    public String showSignUpForm(Model model) {
        model.addAttribute("user", new User());
        return "signup_form";
    }

    @PostMapping("/register")
    public String processRegister(@ModelAttribute User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedpassword = encoder.encode(user.getPassword());
        user.setPassword(encodedpassword);
        userService.save(user);
        return "register_success";
    }

    @GetMapping("/list_users")
    public String viewUserList(Model model) {
        model.addAttribute("userlist", userService.getAllUsers());
        return "users";
    }
}

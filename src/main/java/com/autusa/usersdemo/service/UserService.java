package com.autusa.usersdemo.service;

import com.autusa.usersdemo.model.User;

import java.util.List;

public interface UserService {
    public List<User> getAllUsers();

    public void save(User user);
}

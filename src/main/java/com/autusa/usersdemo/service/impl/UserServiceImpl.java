package com.autusa.usersdemo.service.impl;

import com.autusa.usersdemo.model.User;
import com.autusa.usersdemo.respository.UserRespository;
import com.autusa.usersdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRespository userRespository;

    @Override
    public List<User> getAllUsers() {
        return userRespository.findAll();
    }

    @Override
    public void save(User user) {
        userRespository.save(user);
    }
}
